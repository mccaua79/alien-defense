﻿using System.Linq;
using UnityEngine;

public class EnemyScript : MonoBehaviour {

    private Transform _transform;
    private Rigidbody2D _rb2D;
    public float Speed;
    private Vector3 offset;
    public bool grounded;
    public Transform groundCheck;
    public float groundCheckRadius;
    public LayerMask whatIsGround;
    public float yVelocity;
    private SpriteRenderer _abductionRay;

    // Use this for initialization
    void Start ()
    {
        _transform = GetComponent<Transform>();
        _rb2D = GetComponent<Rigidbody2D>();
        _abductionRay = GetComponentsInChildren<SpriteRenderer>().FirstOrDefault(s => s.name == "Abduction-Ray");
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void FixedUpdate()
    {
        // TODO: This
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);

        if (grounded)
        {
            if (yVelocity > 8f)
            {
                // TODO: Change state to death
            }
            else
            {
                // TODO: Fall state
                // Then transition into walking again
            }

            // if (state == walking)
            _transform.Translate(Speed*Time.deltaTime, 0f, 0f);
        }
        else
        {
            yVelocity = _rb2D.velocity.y;
        }
    }
    void OnMouseDrag()
    {
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
        curPosition.z = 0;
        transform.position = curPosition;
        _rb2D.velocity = new Vector2(0, 0);
    }

    void OnMouseDown()
    {
        _abductionRay.enabled = true;
        // TODO: Create a circle over the object
    }

    void OnMouseUp()
    {
        _abductionRay.enabled = false;
    }
}
